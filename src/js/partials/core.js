$(document).ready(function() { 
  function pageLoad(){
      window.scrollTo(0, 0);

      setTimeout(function() {
        $('body').removeClass('body_active_pre');
        $('.s_header').removeClass('header_pre');
        $('.s_footer').removeClass('footer_pre');

      }, 2000); 
  }
  pageLoad();
   /*resize menu*/
  $('#mobile_btn_menu').on('click', function(){
      $('.mob_nav_wr').toggleClass('nav_menu__active');

      $('.s_header_inner').addClass('mob_menu__active');   



      if($(this).attr('checked')) {
        setTimeout(function() {
          $('.s_header_inner').removeClass('mob_menu__active');   
        }, 500);        
      }      
  });

  $('.main_menu_li').on('click', function() {
    $(this).closest('.header_menu').find('.main_menu_li').removeClass('main_menu_li__active');
    $(this).addClass('main_menu_li__active');
  }); 


  /*form*/
  $("body").on("focus", ".form .bl_fields input", function(){
      $(this).closest(".field_group").addClass("focus").removeClass("has-error");
  });  
  $("body").on("blur", ".form .bl_fields input", function(){
      if( $(this).val().length === 0 ){
         $(this).closest(".field_group").removeClass("focus");
      }      
  });  

  function testInputEmpty(){
    $('bl_fields .field input').each(function(){
      if( $(this).val() != "" ){
        $(this).closest(".field_group").addClass("focus");
      }
    })
  }
  setTimeout(function() {
    testInputEmpty();
  }, 350);  


  $('#fd-form1').on('validation-submit', function(){
    setTimeout(function() {
      $('[data-remodal-id=thanks_popup]').remodal().open();
      $('#fd-form1').get(0).reset();
    }, 100);
  }); 
});

(function() {
  $(function() {
    var $block, $form, $this;
    $block = $('.validation-wrap');
    if (!$block) {
      return;
    }
    $form = $block.find('form');
    $form.submit(function(e) {
      $form.find('.field_group').removeClass('focus has-error--type has-error--required');
  
      var $formWrap, validation;
      e.preventDefault();
      validation = true;
      $(this).find('[required]').each(function() {
        if ($(this).val() === '') {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--required');
        }
      });

      $(this).find('input[type="text"]').each(function() {
        if ($(this).val().length < 2 && !($(this).val() === '')) {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--type').removeClass('has-error--required');
        }
      });
      // $(this).find('select[required]').each(function() {
      //   if (!$(this).find('option:selected').text().length) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error');
      //   }
      // });
      $(this).find('input[type="email"]').each(function() {
        var emailReg;
        if ($(this).val() !== '') {
          emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          if (!emailReg.test($(this).val())) {
            validation = false;
            return $(this).closest('.field_group').addClass('has-error has-error--type').removeClass('has-error--required');
          }
        }
      });      
      // $(this).find('.bl_radiobtns input[required]').each(function() {
      //   if (!$(this).prop("checked")) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error has-error--required');
      //   }
      // });
      $(this).find('.bl_radiobtns input[required]').each(function() {
        if (!$(this).closest(".field_group").find(':checked').length) {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--required');
        }
      });
      // $(this).find('input[type="tel"][required]').each(function() {
      //   if (!$(this).inputmask('isComplete')) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error');
      //   }
      // });
      if (validation) {
        $formWrap = $(this).closest('.validation-wrap');
        
        $formWrap
            .addClass('submitted')
            .removeClass('form-has-error');
          
        return $(this).trigger('validation-submit', $(this));
      } else {
        $(this).closest('.validation-wrap').addClass('form-has-error');
        return $(this).trigger('validation-error', $(this));
      }
    });
    $(document).on('click focus', '.has-error input, .has-error textarea, .has-error .select2', function(e) {
      return $(e.currentTarget).closest('.has-error').removeClass('has-error');
    });
    $this = $(this);
    if ($this.find('.validation-wrap').hasClass('submitted')) {
      $this.find('form').get(0).reset();
    }
    $this.find('.has-error').removeClass('has-error');
    return $this.find('.validation-wrap').removeClass('submitted');
  });

}).call(this);

