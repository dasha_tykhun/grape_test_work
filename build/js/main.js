$(document).ready(function() { 
  function pageLoad(){
      window.scrollTo(0, 0);

      setTimeout(function() {
        $('body').removeClass('body_active_pre');
        $('.s_header').removeClass('header_pre');
        $('.s_footer').removeClass('footer_pre');

      }, 2000); 
  }
  pageLoad();
   /*resize menu*/
  $('#mobile_btn_menu').on('click', function(){
      $('.mob_nav_wr').toggleClass('nav_menu__active');

      $('.s_header_inner').addClass('mob_menu__active');   



      if($(this).attr('checked')) {
        setTimeout(function() {
          $('.s_header_inner').removeClass('mob_menu__active');   
        }, 500);        
      }      
  });

  $('.main_menu_li').on('click', function() {
    $(this).closest('.header_menu').find('.main_menu_li').removeClass('main_menu_li__active');
    $(this).addClass('main_menu_li__active');
  }); 


  /*form*/
  $("body").on("focus", ".form .bl_fields input", function(){
      $(this).closest(".field_group").addClass("focus").removeClass("has-error");
  });  
  $("body").on("blur", ".form .bl_fields input", function(){
      if( $(this).val().length === 0 ){
         $(this).closest(".field_group").removeClass("focus");
      }      
  });  

  function testInputEmpty(){
    $('bl_fields .field input').each(function(){
      if( $(this).val() != "" ){
        $(this).closest(".field_group").addClass("focus");
      }
    })
  }
  setTimeout(function() {
    testInputEmpty();
  }, 350);  


  $('#fd-form1').on('validation-submit', function(){
    setTimeout(function() {
      $('[data-remodal-id=thanks_popup]').remodal().open();
      $('#fd-form1').get(0).reset();
    }, 100);
  }); 
});

(function() {
  $(function() {
    var $block, $form, $this;
    $block = $('.validation-wrap');
    if (!$block) {
      return;
    }
    $form = $block.find('form');
    $form.submit(function(e) {
      $form.find('.field_group').removeClass('focus has-error--type has-error--required');
  
      var $formWrap, validation;
      e.preventDefault();
      validation = true;
      $(this).find('[required]').each(function() {
        if ($(this).val() === '') {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--required');
        }
      });

      $(this).find('input[type="text"]').each(function() {
        if ($(this).val().length < 2 && !($(this).val() === '')) {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--type').removeClass('has-error--required');
        }
      });
      // $(this).find('select[required]').each(function() {
      //   if (!$(this).find('option:selected').text().length) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error');
      //   }
      // });
      $(this).find('input[type="email"]').each(function() {
        var emailReg;
        if ($(this).val() !== '') {
          emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          if (!emailReg.test($(this).val())) {
            validation = false;
            return $(this).closest('.field_group').addClass('has-error has-error--type').removeClass('has-error--required');
          }
        }
      });      
      // $(this).find('.bl_radiobtns input[required]').each(function() {
      //   if (!$(this).prop("checked")) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error has-error--required');
      //   }
      // });
      $(this).find('.bl_radiobtns input[required]').each(function() {
        if (!$(this).closest(".field_group").find(':checked').length) {
          validation = false;
          return $(this).closest('.field_group').addClass('has-error has-error--required');
        }
      });
      // $(this).find('input[type="tel"][required]').each(function() {
      //   if (!$(this).inputmask('isComplete')) {
      //     validation = false;
      //     return $(this).closest('.field_group').addClass('has-error');
      //   }
      // });
      if (validation) {
        $formWrap = $(this).closest('.validation-wrap');
        
        $formWrap
            .addClass('submitted')
            .removeClass('form-has-error');
          
        return $(this).trigger('validation-submit', $(this));
      } else {
        $(this).closest('.validation-wrap').addClass('form-has-error');
        return $(this).trigger('validation-error', $(this));
      }
    });
    $(document).on('click focus', '.has-error input, .has-error textarea, .has-error .select2', function(e) {
      return $(e.currentTarget).closest('.has-error').removeClass('has-error');
    });
    $this = $(this);
    if ($this.find('.validation-wrap').hasClass('submitted')) {
      $this.find('form').get(0).reset();
    }
    $this.find('.has-error').removeClass('has-error');
    return $this.find('.validation-wrap').removeClass('submitted');
  });

}).call(this);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkgeyBcclxuICBmdW5jdGlvbiBwYWdlTG9hZCgpe1xyXG4gICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMCk7XHJcblxyXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQoJ2JvZHknKS5yZW1vdmVDbGFzcygnYm9keV9hY3RpdmVfcHJlJyk7XHJcbiAgICAgICAgJCgnLnNfaGVhZGVyJykucmVtb3ZlQ2xhc3MoJ2hlYWRlcl9wcmUnKTtcclxuICAgICAgICAkKCcuc19mb290ZXInKS5yZW1vdmVDbGFzcygnZm9vdGVyX3ByZScpO1xyXG5cclxuICAgICAgfSwgMjAwMCk7IFxyXG4gIH1cclxuICBwYWdlTG9hZCgpO1xyXG4gICAvKnJlc2l6ZSBtZW51Ki9cclxuICAkKCcjbW9iaWxlX2J0bl9tZW51Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgICAgJCgnLm1vYl9uYXZfd3InKS50b2dnbGVDbGFzcygnbmF2X21lbnVfX2FjdGl2ZScpO1xyXG5cclxuICAgICAgJCgnLnNfaGVhZGVyX2lubmVyJykuYWRkQ2xhc3MoJ21vYl9tZW51X19hY3RpdmUnKTsgICBcclxuXHJcblxyXG5cclxuICAgICAgaWYoJCh0aGlzKS5hdHRyKCdjaGVja2VkJykpIHtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgJCgnLnNfaGVhZGVyX2lubmVyJykucmVtb3ZlQ2xhc3MoJ21vYl9tZW51X19hY3RpdmUnKTsgICBcclxuICAgICAgICB9LCA1MDApOyAgICAgICAgXHJcbiAgICAgIH0gICAgICBcclxuICB9KTtcclxuXHJcbiAgJCgnLm1haW5fbWVudV9saScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcuaGVhZGVyX21lbnUnKS5maW5kKCcubWFpbl9tZW51X2xpJykucmVtb3ZlQ2xhc3MoJ21haW5fbWVudV9saV9fYWN0aXZlJyk7XHJcbiAgICAkKHRoaXMpLmFkZENsYXNzKCdtYWluX21lbnVfbGlfX2FjdGl2ZScpO1xyXG4gIH0pOyBcclxuXHJcblxyXG4gIC8qZm9ybSovXHJcbiAgJChcImJvZHlcIikub24oXCJmb2N1c1wiLCBcIi5mb3JtIC5ibF9maWVsZHMgaW5wdXRcIiwgZnVuY3Rpb24oKXtcclxuICAgICAgJCh0aGlzKS5jbG9zZXN0KFwiLmZpZWxkX2dyb3VwXCIpLmFkZENsYXNzKFwiZm9jdXNcIikucmVtb3ZlQ2xhc3MoXCJoYXMtZXJyb3JcIik7XHJcbiAgfSk7ICBcclxuICAkKFwiYm9keVwiKS5vbihcImJsdXJcIiwgXCIuZm9ybSAuYmxfZmllbGRzIGlucHV0XCIsIGZ1bmN0aW9uKCl7XHJcbiAgICAgIGlmKCAkKHRoaXMpLnZhbCgpLmxlbmd0aCA9PT0gMCApe1xyXG4gICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoXCIuZmllbGRfZ3JvdXBcIikucmVtb3ZlQ2xhc3MoXCJmb2N1c1wiKTtcclxuICAgICAgfSAgICAgIFxyXG4gIH0pOyAgXHJcblxyXG4gIGZ1bmN0aW9uIHRlc3RJbnB1dEVtcHR5KCl7XHJcbiAgICAkKCdibF9maWVsZHMgLmZpZWxkIGlucHV0JykuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICBpZiggJCh0aGlzKS52YWwoKSAhPSBcIlwiICl7XHJcbiAgICAgICAgJCh0aGlzKS5jbG9zZXN0KFwiLmZpZWxkX2dyb3VwXCIpLmFkZENsYXNzKFwiZm9jdXNcIik7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfVxyXG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICB0ZXN0SW5wdXRFbXB0eSgpO1xyXG4gIH0sIDM1MCk7ICBcclxuXHJcblxyXG4gICQoJyNmZC1mb3JtMScpLm9uKCd2YWxpZGF0aW9uLXN1Ym1pdCcsIGZ1bmN0aW9uKCl7XHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAkKCdbZGF0YS1yZW1vZGFsLWlkPXRoYW5rc19wb3B1cF0nKS5yZW1vZGFsKCkub3BlbigpO1xyXG4gICAgICAkKCcjZmQtZm9ybTEnKS5nZXQoMCkucmVzZXQoKTtcclxuICAgIH0sIDEwMCk7XHJcbiAgfSk7IFxyXG59KTtcclxuXHJcbihmdW5jdGlvbigpIHtcclxuICAkKGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyICRibG9jaywgJGZvcm0sICR0aGlzO1xyXG4gICAgJGJsb2NrID0gJCgnLnZhbGlkYXRpb24td3JhcCcpO1xyXG4gICAgaWYgKCEkYmxvY2spIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgJGZvcm0gPSAkYmxvY2suZmluZCgnZm9ybScpO1xyXG4gICAgJGZvcm0uc3VibWl0KGZ1bmN0aW9uKGUpIHtcclxuICAgICAgJGZvcm0uZmluZCgnLmZpZWxkX2dyb3VwJykucmVtb3ZlQ2xhc3MoJ2ZvY3VzIGhhcy1lcnJvci0tdHlwZSBoYXMtZXJyb3ItLXJlcXVpcmVkJyk7XHJcbiAgXHJcbiAgICAgIHZhciAkZm9ybVdyYXAsIHZhbGlkYXRpb247XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgdmFsaWRhdGlvbiA9IHRydWU7XHJcbiAgICAgICQodGhpcykuZmluZCgnW3JlcXVpcmVkXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICB2YWxpZGF0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgICByZXR1cm4gJCh0aGlzKS5jbG9zZXN0KCcuZmllbGRfZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yIGhhcy1lcnJvci0tcmVxdWlyZWQnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgJCh0aGlzKS5maW5kKCdpbnB1dFt0eXBlPVwidGV4dFwiXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCQodGhpcykudmFsKCkubGVuZ3RoIDwgMiAmJiAhKCQodGhpcykudmFsKCkgPT09ICcnKSkge1xyXG4gICAgICAgICAgdmFsaWRhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgcmV0dXJuICQodGhpcykuY2xvc2VzdCgnLmZpZWxkX2dyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvciBoYXMtZXJyb3ItLXR5cGUnKS5yZW1vdmVDbGFzcygnaGFzLWVycm9yLS1yZXF1aXJlZCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIC8vICQodGhpcykuZmluZCgnc2VsZWN0W3JlcXVpcmVkXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIC8vICAgaWYgKCEkKHRoaXMpLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLnRleHQoKS5sZW5ndGgpIHtcclxuICAgICAgLy8gICAgIHZhbGlkYXRpb24gPSBmYWxzZTtcclxuICAgICAgLy8gICAgIHJldHVybiAkKHRoaXMpLmNsb3Nlc3QoJy5maWVsZF9ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcclxuICAgICAgLy8gICB9XHJcbiAgICAgIC8vIH0pO1xyXG4gICAgICAkKHRoaXMpLmZpbmQoJ2lucHV0W3R5cGU9XCJlbWFpbFwiXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGVtYWlsUmVnO1xyXG4gICAgICAgIGlmICgkKHRoaXMpLnZhbCgpICE9PSAnJykge1xyXG4gICAgICAgICAgZW1haWxSZWcgPSAvXihbXFx3LVxcLl0rQChbXFx3LV0rXFwuKStbXFx3LV17Miw0fSk/JC87XHJcbiAgICAgICAgICBpZiAoIWVtYWlsUmVnLnRlc3QoJCh0aGlzKS52YWwoKSkpIHtcclxuICAgICAgICAgICAgdmFsaWRhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXR1cm4gJCh0aGlzKS5jbG9zZXN0KCcuZmllbGRfZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yIGhhcy1lcnJvci0tdHlwZScpLnJlbW92ZUNsYXNzKCdoYXMtZXJyb3ItLXJlcXVpcmVkJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTsgICAgICBcclxuICAgICAgLy8gJCh0aGlzKS5maW5kKCcuYmxfcmFkaW9idG5zIGlucHV0W3JlcXVpcmVkXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIC8vICAgaWYgKCEkKHRoaXMpLnByb3AoXCJjaGVja2VkXCIpKSB7XHJcbiAgICAgIC8vICAgICB2YWxpZGF0aW9uID0gZmFsc2U7XHJcbiAgICAgIC8vICAgICByZXR1cm4gJCh0aGlzKS5jbG9zZXN0KCcuZmllbGRfZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yIGhhcy1lcnJvci0tcmVxdWlyZWQnKTtcclxuICAgICAgLy8gICB9XHJcbiAgICAgIC8vIH0pO1xyXG4gICAgICAkKHRoaXMpLmZpbmQoJy5ibF9yYWRpb2J0bnMgaW5wdXRbcmVxdWlyZWRdJykuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoISQodGhpcykuY2xvc2VzdChcIi5maWVsZF9ncm91cFwiKS5maW5kKCc6Y2hlY2tlZCcpLmxlbmd0aCkge1xyXG4gICAgICAgICAgdmFsaWRhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgcmV0dXJuICQodGhpcykuY2xvc2VzdCgnLmZpZWxkX2dyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvciBoYXMtZXJyb3ItLXJlcXVpcmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgLy8gJCh0aGlzKS5maW5kKCdpbnB1dFt0eXBlPVwidGVsXCJdW3JlcXVpcmVkXScpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIC8vICAgaWYgKCEkKHRoaXMpLmlucHV0bWFzaygnaXNDb21wbGV0ZScpKSB7XHJcbiAgICAgIC8vICAgICB2YWxpZGF0aW9uID0gZmFsc2U7XHJcbiAgICAgIC8vICAgICByZXR1cm4gJCh0aGlzKS5jbG9zZXN0KCcuZmllbGRfZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7XHJcbiAgICAgIC8vICAgfVxyXG4gICAgICAvLyB9KTtcclxuICAgICAgaWYgKHZhbGlkYXRpb24pIHtcclxuICAgICAgICAkZm9ybVdyYXAgPSAkKHRoaXMpLmNsb3Nlc3QoJy52YWxpZGF0aW9uLXdyYXAnKTtcclxuICAgICAgICBcclxuICAgICAgICAkZm9ybVdyYXBcclxuICAgICAgICAgICAgLmFkZENsYXNzKCdzdWJtaXR0ZWQnKVxyXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2Zvcm0taGFzLWVycm9yJyk7XHJcbiAgICAgICAgICBcclxuICAgICAgICByZXR1cm4gJCh0aGlzKS50cmlnZ2VyKCd2YWxpZGF0aW9uLXN1Ym1pdCcsICQodGhpcykpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQodGhpcykuY2xvc2VzdCgnLnZhbGlkYXRpb24td3JhcCcpLmFkZENsYXNzKCdmb3JtLWhhcy1lcnJvcicpO1xyXG4gICAgICAgIHJldHVybiAkKHRoaXMpLnRyaWdnZXIoJ3ZhbGlkYXRpb24tZXJyb3InLCAkKHRoaXMpKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2sgZm9jdXMnLCAnLmhhcy1lcnJvciBpbnB1dCwgLmhhcy1lcnJvciB0ZXh0YXJlYSwgLmhhcy1lcnJvciAuc2VsZWN0MicsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgcmV0dXJuICQoZS5jdXJyZW50VGFyZ2V0KS5jbG9zZXN0KCcuaGFzLWVycm9yJykucmVtb3ZlQ2xhc3MoJ2hhcy1lcnJvcicpO1xyXG4gICAgfSk7XHJcbiAgICAkdGhpcyA9ICQodGhpcyk7XHJcbiAgICBpZiAoJHRoaXMuZmluZCgnLnZhbGlkYXRpb24td3JhcCcpLmhhc0NsYXNzKCdzdWJtaXR0ZWQnKSkge1xyXG4gICAgICAkdGhpcy5maW5kKCdmb3JtJykuZ2V0KDApLnJlc2V0KCk7XHJcbiAgICB9XHJcbiAgICAkdGhpcy5maW5kKCcuaGFzLWVycm9yJykucmVtb3ZlQ2xhc3MoJ2hhcy1lcnJvcicpO1xyXG4gICAgcmV0dXJuICR0aGlzLmZpbmQoJy52YWxpZGF0aW9uLXdyYXAnKS5yZW1vdmVDbGFzcygnc3VibWl0dGVkJyk7XHJcbiAgfSk7XHJcblxyXG59KS5jYWxsKHRoaXMpOyJdLCJmaWxlIjoibWFpbi5qcyJ9
